/**
 * @format
 */
import React from "react";
import { AppRegistry } from "react-native";
import App from "./App/App";
import { name as appName } from "./app.json";
import { withAppContextProvider } from "./App/Context/HomeContext";

AppRegistry.registerComponent(appName, () => withAppContextProvider(App));
