import { createStackNavigator, createAppContainer } from "react-navigation";
import Home from "../screens/Home";

const AppNavigator = createStackNavigator(
  {
    Home: Home
  },
  {
    initialRouteName: "Home",
    navigationOptions: {
      header: null
    }
  }
);

export const AppContainer = createAppContainer(AppNavigator);
