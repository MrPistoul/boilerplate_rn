import Fonts from "./Fonts";
import Metrics from "./Metrics";
import Colors from "./Colors";
import { Platform } from "react-native";
// This file is for a reusable grouping of Theme items.
// Similar to an XML fragment layout in Android
const elevation = Platform.select({
  android: {
    elevation: 5
  },
  ios: {
    shadowColor: "#000",
    shadowOffset: { width: 1, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2
  }
});
const ApplicationStyles = {
  elevation: { ...elevation },
  text: {
    textFoot: {
      fontFamily: Fonts.type.base,
      textAlign: "justify",
      color: Colors.text
    },
    text: {
      fontFamily: Fonts.type.base,
      // fontSize: 17,
      textAlign: "center",
      color: Colors.text
      // flex: 1
    },
    textWhite: {
      fontFamily: Fonts.type.base,
      // fontSize: 17,
      textAlign: "center",
      color: Colors.snow,
      flex: 1
    },
    textSelect: {
      fontFamily: Fonts.type.base,
      // fontSize: 17,
      textAlign: "center",
      color: Colors.secondary,
      flex: 1
    },
    textInput: {
      // fontSize: 15,
      textAlign: "left",
      color: Colors.text
    },
    textInputSnow: {
      // fontSize: 15,
      flex: 1,
      textAlign: "center",
      color: Colors.text
    },
    textLabel: {
      color: Colors.text,
      fontFamily: Fonts.type.bold,
      fontWeight: Platform.OS === "ios" ? "bold" : null,
      // fontSize: 20,
      textAlign: "center"
    },
    textLabelWhite: {
      color: Colors.snow,
      fontFamily: Fonts.type.bold,
      fontWeight: Platform.OS === "ios" ? "bold" : null,
      // fontSize: 20,
      textAlign: "center"
    },
    textList: {
      color: Colors.text,
      fontFamily: Fonts.type.base,
      // fontSize: 15,
      textAlign: "center",
      marginLeft: 1,
      marginRight: 1
    },
    textTitle: {
      fontFamily: Fonts.type.bold,
      fontWeight: Platform.OS === "ios" ? "bold" : null,
      color: Colors.secondary,
      // fontSize: 20,
      textAlign: "center",
      marginTop: 10,
      marginBottom: 10
    },
    textModalTitle: {
      fontFamily: Fonts.type.bold,
      fontWeight: Platform.OS === "ios" ? "bold" : null,
      color: Colors.secondary,
      // fontSize: 23,
      textAlign: "center",
      marginTop: 10,
      marginBottom: 10,
      flex: 1
    }
  },
  badge: {
    badge: {
      position: "absolute",
      right: 0,
      top: 0,
      backgroundColor: "yellow",
      borderRadius: 10,
      borderWidth: 1,
      width: 20,
      height: 20,
      justifyContent: "center",
      alignItems: "center"
    },
    badgeText: { color: Colors.text, fontSize: 12 }
  },
  screen: {
    mainContainer: {
      flex: 1,
      backgroundColor: Colors.transparent
    },
    backgroundImage: {
      position: "absolute",
      top: 0,
      left: 0,
      bottom: 0,
      right: 0
    },
    container: {
      flex: 1,
      paddingTop: Metrics.baseMargin,
      backgroundColor: Colors.transparent
    },
    section: {
      margin: Metrics.section,
      padding: Metrics.baseMargin
    },
    sectionText: {
      ...Fonts.style.normal,
      paddingVertical: Metrics.doubleBaseMargin,
      color: Colors.snow,
      marginVertical: Metrics.smallMargin,
      textAlign: "center"
    },
    subtitle: {
      color: Colors.snow,
      padding: Metrics.smallMargin,
      marginBottom: Metrics.smallMargin,
      marginHorizontal: Metrics.smallMargin
    },
    titleText: {
      ...Fonts.style.h2,
      fontSize: 14,
      color: Colors.text
    }
  },
  darkLabelContainer: {
    padding: Metrics.smallMargin,
    paddingBottom: Metrics.doubleBaseMargin,
    borderBottomColor: Colors.border,
    borderBottomWidth: 1,
    marginBottom: Metrics.baseMargin
  },
  darkLabel: {
    fontFamily: Fonts.type.bold,
    color: Colors.snow
  },
  groupContainer: {
    margin: Metrics.smallMargin,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center"
  },
  sectionTitle: {
    ...Fonts.style.h4,
    color: Colors.coal,
    backgroundColor: Colors.ricePaper,
    padding: Metrics.smallMargin,
    marginTop: Metrics.smallMargin,
    marginHorizontal: Metrics.baseMargin,
    borderWidth: 1,
    borderColor: Colors.ember,
    alignItems: "center",
    textAlign: "center"
  }
};

export default ApplicationStyles;
