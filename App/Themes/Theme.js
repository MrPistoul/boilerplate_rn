import { DefaultTheme, Colors } from "react-native-paper";
import { Platform } from "react-native";
import color from "color";
const fonts = Platform.select({
  web: {
    regular: 'Roboto, "Helvetica Neue", Helvetica, Arial, sans-serif',
    medium:
      'Roboto-Medium, Roboto, "Helvetica Neue", Helvetica, Arial, sans-serif',
    light:
      'Roboto-Light, Roboto, "Helvetica Neue", Helvetica, Arial, sans-serif',
    thin: 'Roboto-Thin, Roboto, "Helvetica Neue", Helvetica, Arial, sans-serif'
  },
  ios: {
    regular: "Helvetica Neue",
    medium: "HelveticaNeue-Medium",
    light: "HelveticaNeue-Light",
    thin: "HelveticaNeue-Thin"
  },
  default: {
    regular: "sans-serif",
    medium: "sans-serif-medium",
    light: "sans-serif-light",
    thin: "sans-serif-thin"
  }
});

const theme = {
  ...DefaultTheme,
  dark: false,
  roundness: 4,
  colors: {
    primary: Colors.teal500,
    accent: Colors.teal200,
    background: Colors.transparent,
    surface: Colors.white,
    error: "#B00020",
    text: Colors.black,
    disabled: color(Colors.black)
      .alpha(0.26)
      .rgb()
      .string(),
    placeholder: color(Colors.black)
      .alpha(0.54)
      .rgb()
      .string(),
    backdrop: color(Colors.black)
      .alpha(0.5)
      .rgb()
      .string(),
    notification: Colors.pink400
  },
  fonts
};

export default theme;
