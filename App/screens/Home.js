import React, { Component } from "react";
import {
  Avatar,
  Button,
  Card,
  Title,
  Paragraph,
  Badge
} from "react-native-paper";
import i18n from "../Locale/i18n";

import { withAppContext } from "../Context/HomeContext";
import { withTheme } from "react-native-paper";

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const { add, remove, count } = this.props.global;
    // const { colors } = this.props.theme; // withTheme HOC

    return (
      <Card style={{ alignSelf: "center", width: "80%", marginTop: "35%" }}>
        <Title>{i18n.t("hello")}</Title>
        <Card.Cover
          source={{
            uri:
              "https://nofilmschool.com/sites/default/files/styles/article_wide/public/big-lebowski-the-dude-1155256-1280x0.jpeg?itok=qt4vmuMH"
          }}
        />
        <Card.Content>
          <Title>MY AWESOME BOILTERPLATE</Title>
          <Paragraph>
            Nobody calls me Lebowsky. You got the wrong guy. I'm the Dude, man.
          </Paragraph>
          <Card.Actions>
            <Button onPress={add}>ADD</Button>
            <Button onPress={remove}>REMOVE</Button>
          </Card.Actions>
          <Badge
            // style={{ backgroundColor: colors.secondary, color: colors.primary }}
            size={40}
          >
            {count}
          </Badge>
        </Card.Content>
      </Card>
    );
  }
}

export default withAppContext(Home);
// export default withTheme(withAppContext(Home));
