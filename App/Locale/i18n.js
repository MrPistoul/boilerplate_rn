import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";

import en from "./translation/en.json";
import fr from "./translation/fr.json";

i18n.fallbacks = true;
i18n.translations = { en, fr };
const fallback = { languageTag: "en", isRTL: false };
const { languageTag, isRTL } =
  RNLocalize.findBestAvailableLanguage(Object.keys(i18n.translations)) ||
  fallback;

// I18nManager.forceRTL(isRTL); // optional, you might not want to handle RTL
i18n.locale = languageTag;

export default i18n;
