import React, { createContext, Component } from "react";

export const HomeContext = createContext({});
// create the consumer as higher order component
export const withAppContext = ChildComponent => props => (
  <HomeContext.Consumer>
    {context => <ChildComponent {...props} global={context} />}
  </HomeContext.Consumer>
);

// create the Provider as higher order component (only for root Component of the application)
export const withAppContextProvider = ChildComponent => props => (
  <HomeProvider>
    <ChildComponent {...props} />
  </HomeProvider>
);
class HomeProvider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0 // une valeur de départ
    };
  }

  add = () => {
    this.setState({
      count: this.state.count + 1
    });
  };
  remove = () => {
    this.setState({
      count: this.state.count - 1
    });
  };
  render() {
    return (
      /**
       * la propriété value est très importante ici, elle rend
       * le contenu du state disponible aux `Consumers` de l'application
       */
      <HomeContext.Provider
        value={{
          count: this.state.count,
          add: this.add,
          remove: this.remove
        }}
      >
        {this.props.children}
      </HomeContext.Provider>
    );
  }
}

export default HomeProvider;
