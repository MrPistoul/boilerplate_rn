/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { Provider as PaperProvider, DefaultTheme } from "react-native-paper";
import { AppContainer } from "./Nav/AppContainer";
import { Theme } from "./Themes/";

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <PaperProvider theme={Theme}>
        <AppContainer />
      </PaperProvider>
    );
  }
}
